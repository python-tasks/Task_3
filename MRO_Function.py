import inspect
class A: pass
class B(A): pass
class C(A): pass
class D(B, C): pass

def mro(class_name):
    mro_list = []
    if class_name in globals() and inspect.isclass(globals()[class_name]):
        inp = globals()[class_name]
        mro_list.append(class_name)
        for i in inp.__bases__:
            if i.__name__ != 'object':  
                mro_list.extend(mro(i.__name__))
            else:
                mro_list.append('object')  
    else:
        print(f"There is no class with name {class_name}")
    mro_list=reversed(mro_list) 
    unique_mro_list=[]  
    for item in mro_list:
        if item not in unique_mro_list:
            unique_mro_list=[item]+unique_mro_list
        
    return unique_mro_list

class_name = input("Please enter class name to print its MRO: ") 
print(mro(class_name))